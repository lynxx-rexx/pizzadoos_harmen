# wat is de basis image waarop je gaat bouwen
FROM python:3.8.3-slim

# maak een folder die working directory wordt
ENV APP_DIR /Lego
RUN mkdir -p ${APP_DIR}
WORKDIR ${APP_DIR}

# kopieer requirements.txt en installeer de gedefinieerde python packages
ADD ./requirements.txt ${APP_DIR}
ADD ./setup.py ${APP_DIR}
RUN pip install -r requirements.txt

# kopieer de source folder naar de map in de container
COPY ./src ${APP_DIR}/src

# definieer wat je gaat runnen na dit script
COPY ./docker-entrypoint.sh ${APP_DIR}/docker-entrypoint.sh
RUN ["chmod", "+x", "/Lego/docker-entrypoint.sh"]
ENTRYPOINT ${APP_DIR}/docker-entrypoint.sh