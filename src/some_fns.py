def add_fish_to_aquarium(fish_list):
    if len(fish_list) > 10:
        raise ValueError("A maximum of 10 jeroens can be added to the aquarium")
    return {"tank_a": fish_list}